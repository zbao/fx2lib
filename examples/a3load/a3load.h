
typedef __bit BOOL;
typedef unsigned char   BYTE;
typedef unsigned short  WORD;
typedef unsigned long   DWORD;

#define TRUE    1
#define FALSE   0

#define bmBIT0   0x01
#define bmBIT1   0x02
#define bmBIT2   0x04
#define bmBIT3   0x08
#define bmBIT4   0x10
#define bmBIT5   0x20
#define bmBIT6   0x40
#define bmBIT7   0x80

#define EZUSB_IRQ_ENABLE()   EUSB = 1
#define EZUSB_IRQ_CLEAR()   EXIF &= ~0x10      // IE2_

#define bmSUDAV         bmBIT0
#define bmEPBUSY        bmBIT1

#define EP0BUFF_SIZE    64
#define VR_UPLOAD       0xc0
#define VR_DOWNLOAD     0x40
#define VR_RAM          0xA3 // loads (uploads) external ram
#define VR_ISFX2        0xAC // returns a single byte (1 if an FX2, 0 otherwise)

__sbit EUSB    = 0xE8+0;
__sbit EA      = 0xA8+7;
__sfr EXIF     = 0x91;

// FX2 Registers
__xdata __at 0xE668 volatile BYTE INTSETUP_FX2             ;         // Interrupt 2&4 Setup
__xdata __at 0xE65C volatile BYTE USBIE_FX2                ;         // USB Int Enables
__xdata __at 0xE65D volatile BYTE USBIRQ_FX2               ;         // USB Interrupt Requests
__xdata __at 0xE6B8 volatile BYTE SETUPDAT_FX2[8]          ;         // 8 bytes of SETUP data
__xdata __at 0xE68A volatile BYTE EP0BCH_FX2               ;         // Endpoint 0 Byte Count H
__xdata __at 0xE68B volatile BYTE EP0BCL_FX2               ;         // Endpoint 0 Byte Count L
__xdata __at 0xE6A0 volatile BYTE EP0CS_FX2                ;         // Endpoint  Control and Status
__xdata __at 0xE740 volatile BYTE EP0BUF_FX2[64]           ;         // EP0 IN-OUT buffer

#define bmHSNAK_FX2      bmBIT7
#define bmAV2EN      bmBIT3

// EZUSB Registers
__xdata __at 0x7FAE volatile BYTE USBIEN_EZUSB            ;
__xdata __at 0x7FAB volatile BYTE USBIRQ_EZUSB            ;
__xdata __at 0x7FE8 volatile BYTE SETUPDAT_EZUSB[8]       ;
__xdata __at 0x7FB4 volatile BYTE EP0CS_EZUSB             ;
__xdata __at 0x7FB5 volatile BYTE IN0BC_EZUSB             ;
__xdata __at 0x7FC5 volatile BYTE OUT0BC_EZUSB            ;
__xdata __at 0x7EC0 volatile BYTE OUT0BUF_EZUSB[64]       ;
__xdata __at 0x7F00 volatile BYTE IN0BUF_EZUSB[64]        ;
__xdata __at 0x7FAF volatile BYTE USBBAV_EZUSB            ;
__xdata __at 0x7F95 volatile BYTE PORTCCFG_EZUSB          ;

#define bmOUTBSY_EZUSB  bmBIT3
#define bmINBSY_EZUSB   bmBIT2
#define bmHSNAK_EZUSB   bmBIT1
